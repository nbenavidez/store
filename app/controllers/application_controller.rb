class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_order


def not_authenticated
  redirect_to login_url, :alert => "First login to access this page."
end


  def current_order
    if !session[:order_id].nil?
      Order.find(session[:order_id])
    else
      Order.new
    end
  end
end
