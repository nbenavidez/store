# Load products
json_data = File.read("db/products.json")
json_products = JSON.parse(json_data)
json_products["products"].each do |attr_hash|
  Product.create!(attr_hash)
end

OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"
